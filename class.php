<?php 
 class Message {
    protected $text = "A simple message";
    public static $count = 0; 
    public function show(){
        echo "<p>$this->text</p>";
    }
    function __construct($text = ""){
       ++self::$count; 
        if($text != ""){
           $this->text = $text; 
       } 
    }   
 }

 class redMessage extends Message {
    public function show(){
        echo "<p style = 'color:red'>$this->text</p>";
    }
 } 

class coloredMessage extends Message {
    protected $color = 'red';
    public function __set($property,$value){
       if ($property == 'color'){
            $colors = array('red','yellow', 'green');
            if(in_array($value, $colors)){
               $this->color = $value;  
            }
       } 
    }
    public function show(){
        echo "<p style = 'color:$this->color'>$this->text</p>";
    }     
}

function showObject($object){
    $object->show(); 
}











//Home exercise 
 class Htmlpage {
    protected $body = 'Body';
    protected $title = 'Title';
    public function show(){
        echo "<html><head><title>$this->title</title></head><body>$this->body</body></html>";
    }
    function __construct($body = "",$title = ""){
        $this->body = $body;
        $this->title = $title;
    }      
 }

 Class coloredPage extends Htmlpage {
    protected $colors = array('red','yellow', 'green');
    protected $color = 'red';
    public function __set($property,$value){
        if ($property == 'color'){
             if(in_array($value, $this->colors)){
                $this->color = $value;  
             } else {
                 die("Please select one of the allowed colors");
             }
        } 
    }
    public function show(){
        echo "<html><head><title>$this->title</title></head><body><p style = 'color:$this->color'>$this->body<p></body></html>";
    }     
    #commnet 1 
    # add more code
    # added code for commit 4
    #yrtyrtyrt 
    #terterterter
}

Class coloredFontdPage extends coloredPage {
    protected $fontSize = 14; 
    public function __set($property,$value){
        if ($property == 'color'){
             if(in_array($value, $this->colors)){
                $this->color = $value;  
             } else {
                 die("Please select one of the allowed colors");
             }
        } elseif ($property == 'fontSize') {
            if (filter_var($value, FILTER_VALIDATE_INT) && $value >= 10 && $value <= 24) {
                $this->fontSize = $value;    
            } else {
                die("Please select one of the allowed font sizes");  
            }
        } 
    }    

    public function show(){
        $fontSize = $this->fontSize.'px';
        echo "<html><head><title>$this->title</title></head><body><p style = 'color:$this->color;font-size:$fontSize'>$this->body<p></body></html>";
    }      

}
?>
