<?php 
    include "class.php";
    include "db.php"; 
    include "query.php";
?>
<html>
  <head>
    <title>Object Oriented PHP</title>
  </head>
  <body>
    <P>
    <?php
      $text = 'Hello World';
      echo "$text And The Uniniverse"; 
      echo '<br>';
      $msg = new Message();
      echo '<br>';
      echo Message::$count;       
      //echo $msg->text; 
      $msg->show();
      $msg1 = new Message("A new text");
      $msg1->show();
      echo '<br>';
      echo Message::$count;      
      $msg2 = new Message(); 
      $msg2->show();
      echo '<br>';
      echo Message::$count;
      echo '<br>';
      $msg3 = new redMessage('A red message');
      $msg3->show(); 
      echo '<br>';
      $msg4 = new coloredMessage('A colored message');
      $msg4->color = 'green';
      $msg4->show(); 
      $msg4->color = 'yellow';
      //echo '<br>';
      //$msg4->show();
      //$msg4->color = 'green';
      //echo '<br>';
      //$msg4->show(); 
      //$msg4->color = 'black';
      //echo '<br>';
      //$msg4->show();
      //echo '<br>';
      //$msg5 = new coloredMessage('A colored message');
      //$msg5->color = 'black'; 
      //$msg5->show(); 
      //$msg5->color = 'green'; 
      //$msg5->color = 'black';  
      //echo '<br>';
      //$msg5->show();  
      //echo '<br>';
      //showObject($msg5);
      //echo '<br>';
      //showObject($msg1);
      //database connection 
      $db = new DB('localhost', 'intro', 'root', '');
      $dbc = $db->connect();
      $query = new Query($dbc); 
      $q = "SELECT * FROM users"; #שאילתא, הכוכבית אומרת בחירת כל העמודות
      $result = $query->query($q); 
      echo '<br>';
      //echo $result->num_rows;
      if($result->num_rows > 0){
        echo '<table>'; 
        echo '<tr><th>Name</th><th>Email</th></tr>';
        while($row = $result->fetch_assoc()){
          echo '<tr>'; 
          echo '<td>'.$row['name'].'</td><td>'.$row['email'].'</td>';
          echo '</tr>';
        }
        echo '</table>';
      } else {
        echo "Sorry no results"; 
      }


    ?>
    </P>
  </body>
</html>
